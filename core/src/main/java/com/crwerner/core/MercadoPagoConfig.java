package com.crwerner.core;

import android.content.Context;

/**
 * Created by cris on 5/4/18.
 */

public class MercadoPagoConfig {
    Context context;
    MercadoPagoAuthConfig authConfig;

    public MercadoPagoConfig() {
    }

    public MercadoPagoConfig(Context context, MercadoPagoAuthConfig authConfig) {
        this.context = context;
        this.authConfig = authConfig;
    }

    public MercadoPagoAuthConfig getAuthConfig() {
        return authConfig;
    }

    public static class Builder{
        Context context;
        MercadoPagoAuthConfig authConfig;

        public Builder(Context context) {
            if (context == null) {
                throw new IllegalArgumentException("Context must not be null.");
            }
            this.context = context;
        }

        public Builder authConfiguration(MercadoPagoAuthConfig authConfig){
            if(authConfig==null){
                throw new IllegalArgumentException("MercadoPagoAuthConfig must not be null.");
            }
            if(authConfig.getApiKey().isEmpty()){
                throw new IllegalArgumentException("Please configure your api key.");
            }

            this.authConfig = authConfig;
            return this;
        }

        public MercadoPagoConfig builder(){
            return new MercadoPagoConfig(context, authConfig);
        }
    }
}


package com.crwerner.core.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class InstallmentsResponse {

    @SerializedName("payment_method_id")
    @Expose
    public String paymentMethodId;
    @SerializedName("payment_type_id")
    @Expose
    public String paymentTypeId;
    @SerializedName("issuer")
    @Expose
    public InstallmentIssuer installmentIssuer;
    @SerializedName("payer_costs")
    @Expose
    public List<PayerCost> payerCosts = null;

}

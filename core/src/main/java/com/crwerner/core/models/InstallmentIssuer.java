
package com.crwerner.core.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InstallmentIssuer {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("is_default")
    @Expose
    public Object isDefault;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("secure_thumbnail")
    @Expose
    public String secureThumbnail;
    @SerializedName("thumbnail")
    @Expose
    public String thumbnail;

}

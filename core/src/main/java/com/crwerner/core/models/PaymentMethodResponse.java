
package com.crwerner.core.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PaymentMethodResponse {

    private final static String CREDIT_CARD = "credit_card";
    private final static String DEBIT_CARD = "debitt_card";

    public enum PaymentType{
        CREDIT_CARD,
        DEBIT_CARD
    }

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("payment_type_id")
    @Expose
    public String paymentTypeId;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("secure_thumbnail")
    @Expose
    public String secureThumbnail;
    @SerializedName("thumbnail")
    @Expose
    public String thumbnail;
    @SerializedName("deferred_capture")
    @Expose
    public String deferredCapture;
    @SerializedName("settings")
    @Expose
    public List<Setting> settings = null;
    @SerializedName("additional_info_needed")
    @Expose
    public List<String> additionalInfoNeeded = null;
    @SerializedName("min_allowed_amount")
    @Expose
    public Double minAllowedAmount;
    @SerializedName("max_allowed_amount")
    @Expose
    public Double maxAllowedAmount;
    @SerializedName("accreditation_time")
    @Expose
    public Double accreditationTime;
    @SerializedName("financial_institutions")
    @Expose
    public List<Object> financialInstitutions = null;

    public static String getPaymentType(PaymentType type){
        switch (type){
            case DEBIT_CARD:return DEBIT_CARD;
            case CREDIT_CARD:return CREDIT_CARD;
            default:return "";
        }
    }

}

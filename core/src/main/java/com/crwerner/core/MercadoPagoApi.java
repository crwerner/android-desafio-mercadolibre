package com.crwerner.core;

/**
 * Created by cris on 5/4/18.
 */

public class MercadoPagoApi {

    public static final String BASE_HOST = "api.mercadopago.com/";
    public static final String BASE_HOST_URL = "https://" + BASE_HOST;

    private final String baseHostUrl;

    public MercadoPagoApi() {
        this(BASE_HOST_URL);
    }

    public MercadoPagoApi(String baseHostUrl) {
        this.baseHostUrl = baseHostUrl;
    }

    public String getBaseHostUrl() {
        return baseHostUrl;
    }
}

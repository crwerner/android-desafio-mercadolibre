package com.crwerner.core;

import android.content.Context;

/**
 * Created by cris on 5/4/18.
 */

public class MercadoPagoAuthConfig {

    private String apiKey;

    public MercadoPagoAuthConfig() {
    }

    public MercadoPagoAuthConfig(String apiKey) {
        this.apiKey = apiKey;
    }


    public String getApiKey() {
        return apiKey;
    }
}

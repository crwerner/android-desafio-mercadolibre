package com.crwerner.core;

import android.content.Context;
import android.support.annotation.NonNull;

/**
 * Created by cris on 5/4/18.
 */

public class MercadoPago {
public static final String TAG = "MercadoPago";
    private static final String NOT_INITIALIZED_MESSAGE = "Must initialize MercadoPago before using getInstance()" ;
    private static final String AUTH_NULL_MESSAGE = "MercadoPagoConfig must not be null." ;
    private static MercadoPago instance;

    MercadoPagoConfig auth;
    Context context;

    public MercadoPago(MercadoPagoConfig auth) {
        if(auth!=null) {
            this.auth = auth;
            this.context = auth.context;
        }else{
            throw new IllegalStateException(AUTH_NULL_MESSAGE);
        }
    }


    public static void initialize(@NonNull MercadoPagoConfig config) {
        createMercadolibre(config);
    }

    static synchronized MercadoPago createMercadolibre(MercadoPagoConfig config) {
        if (instance == null) {
            instance = new MercadoPago(config);
            return instance;
        }

        return instance;
    }


    public static MercadoPago getInstance() {
        checkInitialized();
        return instance;
    }

    static void checkInitialized() {
        if (instance == null) {
            throw new IllegalStateException(NOT_INITIALIZED_MESSAGE);
        }
    }

    public MercadoPagoConfig getAuth() {
        return auth;
    }


}

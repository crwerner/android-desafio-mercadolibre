package com.crwerner.core;

import com.crwerner.core.models.CardIssuersResponse;
import com.crwerner.core.models.InstallmentsResponse;
import com.crwerner.core.models.PaymentMethodResponse;
import com.crwerner.core.services.PaymentMethodService;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by cris on 5/4/18.
 */

public class MercadoPagoApiClient {
    final Retrofit retrofit;

    public MercadoPagoApiClient() {
        retrofit = buildRetrofit(new MercadoPagoApi());
    }


    public void getPaymentMethods(Callback<List<PaymentMethodResponse>> callback){
        new PaymentMethodService().getPaymentMethod(retrofit,
                getApiKey(),
                callback);
    }

    public void getPaymentMethodDebitCard(Callback<List<PaymentMethodResponse>> callback){
        new PaymentMethodService().getPaymentMethodDebitCard(retrofit,
                getApiKey(),
                callback);
    }

    public void getPaymentMethodCreditCard(Callback<List<PaymentMethodResponse>> callback){
        new PaymentMethodService().getPaymentMethodCreditCard(retrofit,
                getApiKey(),
                callback);
    }


    public void getCardIssuers(String paymentMethodId,  Callback<List<CardIssuersResponse>> callback){
        new PaymentMethodService().getCardIssuers(retrofit, getApiKey(), paymentMethodId, callback);
    }

    public void getInstallments( double amount, String paymentMethodId, String issuerId, Callback<List<InstallmentsResponse>> callback){
        new PaymentMethodService().getInstallments(retrofit,getApiKey(),amount, paymentMethodId, issuerId, callback);
    }



    private Retrofit buildRetrofit(MercadoPagoApi mercadoPagoApi){
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        // set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        return new Retrofit.Builder()
                .baseUrl(mercadoPagoApi.getBaseHostUrl())
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();
    }

    private String getApiKey() {
        return MercadoPago.getInstance().getAuth().getAuthConfig().getApiKey();
    }


}

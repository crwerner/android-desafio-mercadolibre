package com.crwerner.core;

import retrofit2.Response;

/**
 * Created by cris on 6/4/18.
 */

public class Result<T> {
    public final T data;
    public final Response response;

    public Result(T data, Response response) {
        this.data = data;
        this.response = response;
    }
}
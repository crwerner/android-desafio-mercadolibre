package com.crwerner.core;

/**
 * Created by cris on 5/4/18.
 */

public class MercadoPagoCore {
    private static MercadoPagoCore instance;
    MercadoPagoApiClient guestClient;

    public static MercadoPagoCore getInstance() {
        if(instance==null){
            instance = new MercadoPagoCore();
        }
        return instance;
    }


    public MercadoPagoApiClient getApiClient(){
        return getGuestApiClient();
    }

    /**
     * Creates {@link MercadoPagoApiClient} using guest authentication.
     *
     * Caches internally for efficient access.
     */
    public MercadoPagoApiClient getGuestApiClient() {
        if (guestClient == null) {
            createGuestClient();
        }

        return guestClient;
    }

    private synchronized void createGuestClient() {
        if (guestClient == null) {
            guestClient = new MercadoPagoApiClient();
        }
    }

    private synchronized void createGuestClient(MercadoPagoApiClient mercadoPagoApiClient) {
        if (guestClient == null) {
            guestClient = mercadoPagoApiClient;
        }
    }

}

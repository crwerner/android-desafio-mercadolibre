package com.crwerner.core.services;

import com.crwerner.core.Callback;
import com.crwerner.core.MercadoPago;
import com.crwerner.core.Result;
import com.crwerner.core.exception.MercadoPagoException;
import com.crwerner.core.models.CardIssuersResponse;
import com.crwerner.core.models.InstallmentsResponse;
import com.crwerner.core.models.PaymentMethodResponse;

import java.util.Iterator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import rx.Observable;

/**
 * Created by cris on 5/4/18.
 */

public class PaymentMethodService {

    public void getPaymentMethod(Retrofit retrofit, String apiKey, Callback<List<PaymentMethodResponse>> callback){
        retrofit.create(IPaymentMethodsService.class).getPaymentMethods(apiKey).enqueue(callback);
    }

    public void getPaymentMethodCreditCard(Retrofit retrofit, String apiKey, final Callback<List<PaymentMethodResponse>> callback){
        retrofit.create(IPaymentMethodsService.class).getPaymentMethods(apiKey).enqueue(new Callback<List<PaymentMethodResponse>>() {
            @Override
            public void success(Result<List<PaymentMethodResponse>> result) {
                if(callback!=null){
                    List<PaymentMethodResponse> creditCards = filterPaymentMethodByType(result.data,
                            PaymentMethodResponse.getPaymentType(PaymentMethodResponse.PaymentType.CREDIT_CARD));

                    callback.success(new Result<>(creditCards,result.response));
                }
            }

            @Override
            public void failure(MercadoPagoException ex) {
                if(callback!=null){
                    callback.failure(ex);
                }
            }
        });
    }

    public void getPaymentMethodDebitCard(Retrofit retrofit, String apiKey, final Callback<List<PaymentMethodResponse>> callback){
        retrofit.create(IPaymentMethodsService.class).getPaymentMethods(apiKey).enqueue(new Callback<List<PaymentMethodResponse>>() {
            @Override
            public void success(Result<List<PaymentMethodResponse>> result) {
                if(callback!=null){
                    List<PaymentMethodResponse> creditCards = filterPaymentMethodByType(result.data,
                            PaymentMethodResponse.getPaymentType(PaymentMethodResponse.PaymentType.DEBIT_CARD));

                    callback.success(new Result<>(creditCards,result.response));
                }
            }

            @Override
            public void failure(MercadoPagoException ex) {
                if(callback!=null){
                    callback.failure(ex);
                }
            }});
    }

    private List<PaymentMethodResponse> filterPaymentMethodByType(List<PaymentMethodResponse> paymentMethodResponses, String type) {
        Iterator<PaymentMethodResponse> iterator = paymentMethodResponses.iterator();
        while (iterator.hasNext()){
            if(!iterator.next().paymentTypeId.equals(type)){
                iterator.remove();
            }
        }
        return paymentMethodResponses;
    }

    public void getCardIssuers(Retrofit retrofit, String apiKey, String paymentMethodId, Callback<List<CardIssuersResponse>> callback){
        retrofit.create(IPaymentMethodsService.class).getCardIssuers(apiKey,paymentMethodId).enqueue(callback);
    }

    public void getInstallments(Retrofit retrofit, String apiKey, double amount, String paymentMethodId, String issuerId, Callback<List<InstallmentsResponse>> callback){
        retrofit.create(IPaymentMethodsService.class).getInstallments(apiKey, amount, paymentMethodId, issuerId).enqueue(callback);
    }

}

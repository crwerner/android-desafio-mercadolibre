package com.crwerner.core.services;

import com.crwerner.core.models.CardIssuersResponse;
import com.crwerner.core.models.InstallmentsResponse;
import com.crwerner.core.models.PaymentMethodResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by cris on 5/4/18.
 */

public interface IPaymentMethodsService {

    @GET("v1/payment_methods")
    @Headers({"Accept: application/json"})
    Call<List<PaymentMethodResponse>> getPaymentMethods(@Query("public_key") String key);

    @GET("v1/payment_methods/card_issuers")
    @Headers({"Accept: application/json"})
    Call<List<CardIssuersResponse>> getCardIssuers(@Query("public_key") String key, @Query("payment_method_id") String paymentMethodId);

    @GET("v1/payment_methods/installments")
    @Headers({"Accept: application/json"})
    Call<List<InstallmentsResponse>> getInstallments(@Query("public_key") String key,
                                                     @Query("amount") double amount,
                                                     @Query("payment_method_id") String paymentMethodId,
                                                     @Query("issuer.id") String issuerId
                                                     );

}

package com.crwerner.core;

import com.crwerner.core.exception.MercadoPagoApiException;
import com.crwerner.core.exception.MercadoPagoException;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by cris on 6/4/18.
 */

public abstract class  Callback<T> implements retrofit2.Callback<T> {
    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        if (response.isSuccessful()) {
            success(new Result<>(response.body(), response));
        } else {
            failure(new MercadoPagoApiException(response));
        }
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        failure(new MercadoPagoException("Request Failure", t));
    }


    /**
     * Called when call completes successfully.
     *
     * @param result the parsed result.
     */
    public abstract void success(Result<T> result);

    /**
     * Unsuccessful call due to network failure, non-2XX status code, or unexpected
     * exception.
     */
    public abstract void failure(MercadoPagoException ex);
}

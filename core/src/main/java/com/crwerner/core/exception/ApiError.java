package com.crwerner.core.exception;

import com.google.gson.annotations.SerializedName;

/**
 * Created by cris on 6/4/18.
 */

public class ApiError {

    @SerializedName("message")
    public final String message;

    @SerializedName("code")
    public final int code;

    public ApiError(String message, int code) {
        this.message = message;
        this.code = code;
    }
}
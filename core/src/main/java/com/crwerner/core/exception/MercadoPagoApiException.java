package com.crwerner.core.exception;

import android.text.TextUtils;
import android.util.Log;

import com.crwerner.core.MercadoPago;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

import retrofit2.Response;

/**
 * Created by cris on 6/4/18.
 */

public class MercadoPagoApiException extends MercadoPagoException {

    public static final int DEFAULT_ERROR_CODE = 0;
    private final ApiError apiError;
    private final int code;
    private final Response response;

    public MercadoPagoApiException(Response response) {
        this(response, readApiError(response),  response.code());
    }

    public MercadoPagoApiException(Response response, ApiError apiError, int code) {
        super(createExceptionMessage(code));
        this.apiError = apiError;
        this.code = code;
        this.response = response;
    }

    public int getStatusCode() {
        return code;
    }

    public int getErrorCode() {
        return apiError == null ? DEFAULT_ERROR_CODE : apiError.code;
    }

    public String getErrorMessage() {
        return apiError == null ? null : apiError.message;
    }



    public Response getResponse() {
        return response;
    }


    public static ApiError readApiError(Response response) {
        try {
            // The response buffer can only be read once, so we clone the underlying buffer so the
            // response can be consumed down stream if necessary.
            final String body = response.errorBody().source().buffer().clone().readUtf8();
            if (!TextUtils.isEmpty(body)) {
                return parseApiError(body);
            }
        } catch (Exception e) {
           Log.e(MercadoPago.TAG, "Unexpected response", e);
        }

        return null;
    }

    static ApiError parseApiError(String body) {
        final Gson gson = new GsonBuilder().create();
        try {
            final ApiErrors apiErrors = gson.fromJson(body, ApiErrors.class);
            if (!apiErrors.errors.isEmpty()) {
                return apiErrors.errors.get(0);
            }
        } catch (JsonSyntaxException e) {
            Log.e(MercadoPago.TAG, "Invalid json: " + body, e);
        }
        return null;
    }

    static String createExceptionMessage(int code) {
        return "HTTP request failed, Status: " + code;
    }
}

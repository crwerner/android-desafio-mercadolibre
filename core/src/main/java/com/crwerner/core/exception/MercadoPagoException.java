package com.crwerner.core.exception;

/**
 * Created by cris on 6/4/18.
 */

public class MercadoPagoException  extends RuntimeException {

    public MercadoPagoException(String message) {
        super(message);
    }

    public MercadoPagoException(String message, Throwable cause) {
        super(message, cause);
    }
}

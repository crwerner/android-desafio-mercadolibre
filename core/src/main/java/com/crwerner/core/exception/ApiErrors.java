package com.crwerner.core.exception;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by cris on 6/4/18.
 */

public class ApiErrors {
    @SerializedName("errors")
    public final List<ApiError> errors;

    private ApiErrors() {
        this(null);
    }

    public ApiErrors(List<ApiError> errors) {
        this.errors = errors;
    }
}

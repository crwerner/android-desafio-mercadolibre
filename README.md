# MERCADO PAGO DESAFIO

Este proyecto tiene como objetivo implementar las api call de MercadoPago.
Devolviendo los siguientes servicios:
 *    **Metodos de pago.**
 *    **Entidades emisoras.**
 *    **Cuotas según el metodo de pago y entidades seleccionadas.**
 
El proyecto se divide en dos módulos principales (core y app).

## CORE
Fue desarrollado con la intención de que funcione como un SDK de MercadoPago. Este módulo proporciona métodos para acceder a la API a partir de una 'public_key' proporcionada.
Además de un acceso a las apps por medio de una sola clase 'MercadoPagoCore' donde se encuentran las llamadas a los servicios de MercadoPago.
El SDK proporciona Callbacks conocidas para la recepción de los resultados y manejo de excepciones propias.

## APP
La aplicación fue desarrollada implementando el patron MVP.

## COMO USAR?
### Inicialización de MercadoPago SDK
Ya sea en un archivo de aplicacion o en la MainActivity llamar a:
``` 
	MercadoPagoAuthConfig authConfig = new MercadoPagoAuthConfig(CLIENT_API_KEY);
    MercadoPagoConfig config = new MercadoPagoConfig.Builder(context)
                .authConfiguration(authConfig).builder();
	MercadoPago.initialize(config);
```
### Llamadas a los servicios de la API

**Call List of PaymentMethods**
```
 		MercadoPagoCore.getInstance().getApiClient().getPaymentMethodCreditCard(new Callback<List<PaymentMethodResponse>>() {
            @Override
            public void success(Result<List<PaymentMethodResponse>> result) {
                //TODO: Implement you code
			}

            @Override
            public void failure(MercadoPagoException ex) {
				//TODO: Implement you code
            }
        });		
```

**Call List of CardIssuers**
```
        MercadoPagoCore.getInstance().getApiClient().getCardIssuers(paymentMethodId, new Callback<List<CardIssuersResponse>>() {
            @Override
            public void success(Result<List<CardIssuersResponse>> result) {
                //TODO: Implement you code
            }

            @Override
            public void failure(MercadoPagoException ex) {
                //TODO: Implement you code
        });
```
**Call Installments**
```
     MercadoPagoCore.getInstance().getApiClient().getInstallments(amount, paymentMethodModel.getId(), cardIssuerModel.getId(), new Callback<List<InstallmentsResponse>>() {
            @Override
            public void success(Result<List<InstallmentsResponse>> result) {
                //TODO: Implement you code
            }

            @Override
            public void failure(MercadoPagoException ex) {
                //TODO: Implement you code
        });
```
## Prerrequisitos
	* Version de compilacion de android (compileSdkVersion 26)
	
## Autor

* **Cristian Werner**

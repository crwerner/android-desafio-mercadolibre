package com.crwerner.desafiomercadolibre.amount.presenter;

import com.crwerner.desafiomercadolibre.amount.view.IAmountView;

/**
 * Created by cris on 8/4/18.
 */

public class AmountPresenter implements IAmountPresenter {
    public static final int AMOUNT_REQUIRED = 0;
    public static final int AMOUNT_LESS_ZERO = 1;

    IAmountView iAmountView;

    public AmountPresenter(IAmountView iAmountView) {
        this.iAmountView = iAmountView;
    }

    @Override
    public void validateData(String amount) {
        if(iAmountView!=null) {
            if (amount.isEmpty()) {
                iAmountView.onAmountError(AMOUNT_REQUIRED);

            } else {
                double ret = Double.parseDouble(amount);
                if (ret <= 0) {
                    iAmountView.onAmountError(AMOUNT_LESS_ZERO);
                }else{
                    iAmountView.onAmountCorrect(ret);
                }
            }
        }
    }
}

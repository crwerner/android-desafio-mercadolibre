package com.crwerner.desafiomercadolibre.amount.presenter;

/**
 * Created by cris on 8/4/18.
 */

public interface IAmountPresenter {
    void validateData(String amount);
}

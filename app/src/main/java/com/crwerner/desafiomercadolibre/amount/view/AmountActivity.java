package com.crwerner.desafiomercadolibre.amount.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.crwerner.desafiomercadolibre.BaseActivity;
import com.crwerner.desafiomercadolibre.IView;
import com.crwerner.desafiomercadolibre.R;
import com.crwerner.desafiomercadolibre.amount.presenter.AmountPresenter;
import com.crwerner.desafiomercadolibre.cardissuers.model.CardIssuerModel;
import com.crwerner.desafiomercadolibre.installments.model.PayerCostModel;
import com.crwerner.desafiomercadolibre.paymentmethods.model.PaymentMethodModel;
import com.crwerner.desafiomercadolibre.paymentmethods.view.PaymentMethodsActivity;
import com.crwerner.desafiomercadolibre.result.ResultFragment;
import com.crwerner.desafiomercadolibre.result.model.ResultModel;

import org.parceler.Parcels;

public class AmountActivity extends BaseActivity implements IAmountView {
    private final static String TAG = "AmountActivity";
    public final static String ARG_PAYMENT_METHOD="payment_method";
    public final static String ARG_CARD_ISSUER="card_issuer";
    public final static String ARG_PAYER_COST="payer_cost";
    private static final int REQ_PAYMENT_METHODS = 99;

    Button btnOpenPaymentMethods;
    EditText tvAmount;

    AmountPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_amount);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        findAndInitializeViews();
        this.presenter = new AmountPresenter(this);
    }

    @Override
    public void findAndInitializeViews() {
        setupToolbar();
        tvAmount = findViewById(R.id.tv_amount);
        btnOpenPaymentMethods = findViewById(R.id.btn_open_payment_methods);
        btnOpenPaymentMethods.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            presenter.validateData(tvAmount.getText().toString());
            }
        });
    }

    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK){
            if(requestCode == REQ_PAYMENT_METHODS){
                if(data!=null){
                    showResultSelectionInstallment(data);
                }

            }
        }
    }

    private void showResultSelectionInstallment(Intent data){
        if(data.hasExtra(ARG_PAYER_COST)){

            PayerCostModel payerCostModel = Parcels.unwrap(data.getParcelableExtra(ARG_PAYER_COST));

            CardIssuerModel cardIssuerModel = Parcels.unwrap(data.getParcelableExtra(ARG_CARD_ISSUER));

            PaymentMethodModel paymentMethodModel = Parcels.unwrap(data.getParcelableExtra(ARG_PAYMENT_METHOD));

            ResultModel resultModel = new ResultModel();
            resultModel.setCardIssuerModel(cardIssuerModel);
            resultModel.setPayerCost(payerCostModel);
            resultModel.setPaymentMethodModel(paymentMethodModel);

            ResultFragment resultFragment = ResultFragment.newInstance(resultModel);


            resultFragment.show(getSupportFragmentManager().beginTransaction(), ResultFragment.TAG);
        }
    }


    @Override
    public void onAmountError(int errorCode) {
        if(errorCode == AmountPresenter.AMOUNT_REQUIRED) {
            tvAmount.setError(getString(R.string.input_amount_required));
        }else if(errorCode == AmountPresenter.AMOUNT_LESS_ZERO){
            tvAmount.setError(getString(R.string.error_amount_less_zero));
        }
    }

    @Override
    public void onAmountCorrect(double amount) {
        Intent intent = PaymentMethodsActivity.getCallingIntent(AmountActivity.this,amount);
        transitionForResultTo(intent, REQ_PAYMENT_METHODS);
    }
}

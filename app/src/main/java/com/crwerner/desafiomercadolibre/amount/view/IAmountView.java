package com.crwerner.desafiomercadolibre.amount.view;

import com.crwerner.desafiomercadolibre.IView;

/**
 * Created by cris on 8/4/18.
 */

public interface IAmountView extends IView {
    void onAmountError(int errorCode);
    void onAmountCorrect(double amount);
}

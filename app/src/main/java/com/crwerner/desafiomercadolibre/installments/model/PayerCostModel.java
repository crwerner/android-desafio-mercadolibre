package com.crwerner.desafiomercadolibre.installments.model;

import org.parceler.Parcel;

/**
 * Created by cris on 7/4/18.
 */
@Parcel
public class PayerCostModel {
    int installments;
    String message;
    double total;

    public int getInstallments() {
        return installments;
    }

    public String getMessage() {
        return message;
    }

    public double getTotal() {
        return total;
    }

    public void setInstallments(int installments) {
        this.installments = installments;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setTotal(double total) {
        this.total = total;
    }
}

package com.crwerner.desafiomercadolibre.installments.presenter;

/**
 * Created by cris on 7/4/18.
 */

public interface IInstallmentsPresenter {
    void loadInstallments(double amount);
}

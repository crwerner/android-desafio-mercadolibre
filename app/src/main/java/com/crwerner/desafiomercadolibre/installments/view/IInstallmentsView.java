package com.crwerner.desafiomercadolibre.installments.view;

import com.crwerner.desafiomercadolibre.IView;
import com.crwerner.desafiomercadolibre.installments.model.InstallmentModel;
import com.crwerner.desafiomercadolibre.installments.model.PayerCostModel;

import java.util.List;

/**
 * Created by cris on 7/4/18.
 */

public interface IInstallmentsView extends IView {
    void onLoadInstallmentssFailure(String message);
    void onLoadInstallmentsSuccess(List<PayerCostModel> payerCostModelList);
    void onEmptyData();
}

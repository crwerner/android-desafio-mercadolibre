package com.crwerner.desafiomercadolibre.installments.presenter;

import com.crwerner.core.Callback;
import com.crwerner.core.MercadoPagoCore;
import com.crwerner.core.Result;
import com.crwerner.core.exception.MercadoPagoException;
import com.crwerner.core.models.InstallmentsResponse;
import com.crwerner.core.models.PayerCost;
import com.crwerner.desafiomercadolibre.cardissuers.model.CardIssuerModel;
import com.crwerner.desafiomercadolibre.installments.model.InstallmentModel;
import com.crwerner.desafiomercadolibre.installments.model.PayerCostModel;
import com.crwerner.desafiomercadolibre.installments.view.IInstallmentsView;
import com.crwerner.desafiomercadolibre.paymentmethods.model.PaymentMethodModel;
import com.crwerner.desafiomercadolibre.util.MapperUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cris on 7/4/18.
 */

public class InstallmentsPresenter extends MapperUtil<InstallmentModel, InstallmentsResponse> implements IInstallmentsPresenter {

    PaymentMethodModel paymentMethodModel;
    CardIssuerModel cardIssuerModel;
    IInstallmentsView iView;

    public InstallmentsPresenter(PaymentMethodModel paymentMethodModel, CardIssuerModel cardIssuerModel, IInstallmentsView iView) {
        this.paymentMethodModel = paymentMethodModel;
        this.cardIssuerModel = cardIssuerModel;
        this.iView = iView;
    }

    @Override
    public void loadInstallments(double amount) {
        if(iView!=null)iView.showProgress();
        MercadoPagoCore.getInstance().getApiClient().getInstallments(amount, paymentMethodModel.getId(), cardIssuerModel.getId(), new Callback<List<InstallmentsResponse>>() {
            @Override
            public void success(Result<List<InstallmentsResponse>> result) {
                if(iView!=null){
                    resultData(result.data);
                }
            }

            @Override
            public void failure(MercadoPagoException ex) {
                if(iView!=null){
                    iView.hideProgress();
                    iView.onLoadInstallmentssFailure(ex.getLocalizedMessage());
                }
            }
        });
    }

    private void resultData(List<InstallmentsResponse> data) {

        if(!data.isEmpty()) {
            List<PayerCostModel> ret = new ArrayList<>();
            List<InstallmentModel> transformedData = transform(data);
            for (InstallmentModel installmentModel : transformedData) {
                ret.addAll(installmentModel.getPayerCosts());
            }
            iView.onLoadInstallmentsSuccess(ret);
        }else{
            iView.onEmptyData();
        }
        iView.hideProgress();
    }

    @Override
    public InstallmentModel transform(InstallmentsResponse source) {
        InstallmentModel ret = new InstallmentModel();
        ret.setCardIssuerModel(cardIssuerModel);
        ret.setPaymentMethodModel(paymentMethodModel);
        ret.setPayerCosts(new PayerCostPresenter().transform(source.payerCosts));
        return ret;
    }
}

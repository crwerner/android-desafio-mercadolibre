package com.crwerner.desafiomercadolibre.installments.view.adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.crwerner.desafiomercadolibre.R;
import com.crwerner.desafiomercadolibre.installments.model.PayerCostModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cris on 6/4/18.
 */

public class PayerCostAdapter extends RecyclerView.Adapter<PayerCostAdapter.ViewHolder>{
    List<PayerCostModel> payerCostModels;
    Listener listener;
    View view;

    public PayerCostAdapter() {
        payerCostModels = new ArrayList<>();
    }

    public PayerCostAdapter(List<PayerCostModel> payerCostModels) {
        this.payerCostModels = payerCostModels;
    }

    public void setPayerCostModels(List<PayerCostModel> payerCostModels) {
        this.payerCostModels = payerCostModels;
        notifyDataSetChanged();
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view= LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_payer_cost, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final PayerCostModel model = payerCostModels.get(position);

        holder.tvMessage.setText(model.getMessage());

        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(listener!=null){
                    listener.onItemSelected(model);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return payerCostModels.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        CardView container;
        TextView tvMessage;
        public ViewHolder(View itemView) {
            super(itemView);
            container =itemView.findViewById(R.id.container);
            tvMessage =itemView.findViewById(R.id.tv_message);
        }
    }

    public interface Listener{
        void onItemSelected(PayerCostModel payerCostModel);
    }


}

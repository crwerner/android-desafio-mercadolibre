package com.crwerner.desafiomercadolibre.installments.presenter;

import com.crwerner.core.models.PayerCost;
import com.crwerner.desafiomercadolibre.installments.model.PayerCostModel;
import com.crwerner.desafiomercadolibre.util.MapperUtil;

/**
 * Created by cris on 7/4/18.
 */

public class PayerCostPresenter extends MapperUtil<PayerCostModel, PayerCost> {
    @Override
    public PayerCostModel transform(PayerCost source) {
        PayerCostModel ret = new PayerCostModel();
        ret.setInstallments(source.installments);
        ret.setMessage(source.recommendedMessage);
        ret.setTotal(source.totalAmount);
        return ret;
    }
}

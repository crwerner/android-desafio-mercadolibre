package com.crwerner.desafiomercadolibre.installments.model;

import com.crwerner.desafiomercadolibre.cardissuers.model.CardIssuerModel;
import com.crwerner.desafiomercadolibre.paymentmethods.model.PaymentMethodModel;

import org.parceler.Parcel;

import java.util.List;

/**
 * Created by cris on 7/4/18.
 */
@Parcel
public class InstallmentModel {
    PaymentMethodModel paymentMethodModel;
    CardIssuerModel cardIssuerModel;
    List<PayerCostModel> payerCosts;

    public PaymentMethodModel getPaymentMethodModel() {
        return paymentMethodModel;
    }

    public CardIssuerModel getCardIssuerModel() {
        return cardIssuerModel;
    }

    public List<PayerCostModel> getPayerCosts() {
        return payerCosts;
    }

    public void setPaymentMethodModel(PaymentMethodModel paymentMethodModel) {
        this.paymentMethodModel = paymentMethodModel;
    }

    public void setCardIssuerModel(CardIssuerModel cardIssuerModel) {
        this.cardIssuerModel = cardIssuerModel;
    }

    public void setPayerCosts(List<PayerCostModel> payerCosts) {
        this.payerCosts = payerCosts;
    }
}

package com.crwerner.desafiomercadolibre.installments.view;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;

import com.crwerner.desafiomercadolibre.BaseActivity;
import com.crwerner.desafiomercadolibre.amount.view.AmountActivity;
import com.crwerner.desafiomercadolibre.R;
import com.crwerner.desafiomercadolibre.cardissuers.model.CardIssuerModel;
import com.crwerner.desafiomercadolibre.controller.AlertController;
import com.crwerner.desafiomercadolibre.installments.model.PayerCostModel;
import com.crwerner.desafiomercadolibre.installments.presenter.InstallmentsPresenter;
import com.crwerner.desafiomercadolibre.installments.view.adapter.PayerCostAdapter;
import com.crwerner.desafiomercadolibre.paymentmethods.model.PaymentMethodModel;

import org.parceler.Parcels;

import java.util.List;

/**
 * Created by cris on 7/4/18.
 */

public class InstallmentsActivity extends BaseActivity implements IInstallmentsView {

    private final static String ARG_AMOUNT="amount";
    private final static String ARG_PAYMENT_METHOD="payment_method";
    private final static String ARG_CARD_ISSUER="card_issuer";

    InstallmentsPresenter presenter;
    PayerCostAdapter adapter;

    double amount;
    PaymentMethodModel paymentMethodModel;
    CardIssuerModel cardIssuerModel;


    RecyclerView recyclerList;
    ProgressBar progressList;
    Toolbar toolbar;
    AlertController alertController;

    public static Intent getCallingInent(Context context, double amount, PaymentMethodModel paymentMethodModel, CardIssuerModel cardIssuerModel){
        Intent intent = new Intent(context, InstallmentsActivity.class);
        intent.putExtra(ARG_AMOUNT, amount);
        intent.putExtra(ARG_PAYMENT_METHOD, Parcels.wrap(paymentMethodModel));
        intent.putExtra(ARG_CARD_ISSUER, Parcels.wrap(cardIssuerModel));

        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_installments);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setupAnimation();
        }

        findAndInitializeViews();

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        loadDataFromIntentExtras(savedInstanceState);

        this.presenter = new InstallmentsPresenter(paymentMethodModel, cardIssuerModel, this);
        this.presenter.loadInstallments(amount);

    }

    private void loadDataFromIntentExtras(@Nullable Bundle savedInstanceState) {
        if(savedInstanceState!=null) {
            this.amount = savedInstanceState.getDouble(ARG_AMOUNT, 0);
            this.paymentMethodModel = Parcels.unwrap(savedInstanceState.getParcelable(ARG_PAYMENT_METHOD));
            this.cardIssuerModel = Parcels.unwrap(savedInstanceState.getParcelable(ARG_CARD_ISSUER));
        }else{
            this.amount = getIntent().getDoubleExtra(ARG_AMOUNT, 0);
            this.paymentMethodModel = Parcels.unwrap(getIntent().getParcelableExtra(ARG_PAYMENT_METHOD));
            this.cardIssuerModel = Parcels.unwrap(getIntent().getParcelableExtra(ARG_CARD_ISSUER));
        }
    }

    @Override
    public void findAndInitializeViews() {
        this.toolbar = findViewById(R.id.toolbar);
        this.recyclerList = findViewById(R.id.recycler_list);
        this.progressList = findViewById(R.id.progress_list);
        this.alertController = findViewById(R.id.alert_controller);

        progressList.setVisibility(View.GONE);

        setupToolbar();
        setupRecycler();
    }

    @Override
    public void showProgress() {
        progressList.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressList.setVisibility(View.GONE);
    }

    @Override
    public void onLoadInstallmentssFailure(String message) {
        alertController.setMessage(getString(R.string.alert_installments_error))
                .showActionButton(true)
                .configureAction( getString(R.string.retry), new AlertController.Listener() {
                    @Override
                    public void onActionListener() {
                        presenter.loadInstallments(amount);
                        alertController.hide();
                    }
                }).show();
    }

    @Override
    public void onLoadInstallmentsSuccess(List<PayerCostModel> payerCostModelList) {
        recyclerList.setVisibility(View.VISIBLE);
        adapter.setPayerCostModels(payerCostModelList);
        alertController.hide();
    }


    @Override
    public void onEmptyData() {
        this.alertController.setMessage(getString(R.string.alert_installments_empty)).showActionButton(false).show();
    }


    private void setupRecycler(){
        this.adapter = new PayerCostAdapter();
        this.adapter.setListener(new PayerCostAdapter.Listener() {
            @Override
            public void onItemSelected(PayerCostModel payerCostModel) {
                resultSelection(payerCostModel);
            }
        });
        this.recyclerList.setLayoutManager(new LinearLayoutManager(this));
        this.recyclerList.setAdapter(adapter);
    }

    private void setupToolbar(){

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        toolbar.setTitle(R.string.title_installments);
    }

    private void resultSelection(PayerCostModel payerCostModel){
        Intent intent = new Intent();
        intent.putExtra(AmountActivity.ARG_CARD_ISSUER, Parcels.wrap(cardIssuerModel));
        intent.putExtra(AmountActivity.ARG_PAYMENT_METHOD, Parcels.wrap(paymentMethodModel));
        intent.putExtra(AmountActivity.ARG_PAYER_COST, Parcels.wrap(payerCostModel));
        setResult(RESULT_OK, intent);
        finish();

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putDouble(ARG_AMOUNT, amount);
        outState.putParcelable(ARG_PAYMENT_METHOD, Parcels.wrap(paymentMethodModel));
        outState.putParcelable(ARG_CARD_ISSUER, Parcels.wrap(cardIssuerModel));
        super.onSaveInstanceState(outState);
    }
}

package com.crwerner.desafiomercadolibre.util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cris on 6/4/18.
 */

public abstract class MapperUtil<T, U>  {

    public List<T> transform(List<U> sources) {

        List<T> targets = new ArrayList<>();
        if(sources != null) {
            for (U source : sources) {
                T target = transform(source);
                if (target != null) {
                    targets.add(target);
                }
            }
        }
        return targets;
    }

    public abstract T transform(U source);
}

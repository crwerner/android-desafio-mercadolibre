package com.crwerner.desafiomercadolibre;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.transition.Explode;
import android.transition.Fade;
import android.transition.Slide;
import android.transition.Visibility;
import android.view.Gravity;
import android.view.View;

/**
 * Created by cris on 8/4/18.
 */
public class BaseActivity extends AppCompatActivity {




    @SuppressWarnings("unchecked")public void transitionTo(Intent i) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            try {
                final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(this, true);
                ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(this, pairs);
                startActivity(i, transitionActivityOptions.toBundle());
            }catch (Exception ex){
                startActivity(i);
            }
        }else{
            startActivity(i);
        }
    }



    @SuppressLint("RestrictedApi")
    @SuppressWarnings("unchecked")public void transitionForResultTo(Intent i, int requestCode) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            try {
                final Pair<View, String>[] pairs = TransitionHelper.createSafeTransitionParticipants(this, true);
                ActivityOptionsCompat transitionActivityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(this, pairs);
                startActivityForResult(i, requestCode, transitionActivityOptions.toBundle());
            }catch (Exception ex){
                startActivityForResult(i, requestCode);
            }
        }else{
            startActivityForResult(i, requestCode);
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    protected void setupAnimation(){
        Slide slideTransition = new Slide(Gravity.LEFT);
        slideTransition.setDuration(300);


        getWindow().setEnterTransition(slideTransition);
    }

}

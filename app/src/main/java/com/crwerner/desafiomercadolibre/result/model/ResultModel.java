package com.crwerner.desafiomercadolibre.result.model;

import com.crwerner.desafiomercadolibre.cardissuers.model.CardIssuerModel;
import com.crwerner.desafiomercadolibre.installments.model.PayerCostModel;
import com.crwerner.desafiomercadolibre.paymentmethods.model.PaymentMethodModel;

import org.parceler.Parcel;

/**
 * Created by cris on 8/4/18.
 */

@Parcel
public class ResultModel {
    PaymentMethodModel paymentMethodModel;
    CardIssuerModel cardIssuerModel;
    PayerCostModel payerCost;

    public void setPaymentMethodModel(PaymentMethodModel paymentMethodModel) {
        this.paymentMethodModel = paymentMethodModel;
    }

    public void setCardIssuerModel(CardIssuerModel cardIssuerModel) {
        this.cardIssuerModel = cardIssuerModel;
    }

    public void setPayerCost(PayerCostModel payerCost) {
        this.payerCost = payerCost;
    }

    public PaymentMethodModel getPaymentMethodModel() {
        return paymentMethodModel;
    }

    public CardIssuerModel getCardIssuerModel() {
        return cardIssuerModel;
    }

    public PayerCostModel getPayerCost() {
        return payerCost;
    }
}

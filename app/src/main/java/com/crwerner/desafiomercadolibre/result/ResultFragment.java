package com.crwerner.desafiomercadolibre.result;

import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.crwerner.desafiomercadolibre.IView;
import com.crwerner.desafiomercadolibre.R;
import com.crwerner.desafiomercadolibre.result.model.ResultModel;

import org.parceler.Parcels;

import java.util.List;

/**
 * Created by cris on 8/4/18.
 */

public class ResultFragment extends DialogFragment implements IView{
    public static final String TAG = "ResultFragment";
    private static final String ARG_RESULT_MODEL = "result_model";
    View view;
    ResultModel resultModel;

    Button btnFinish;
    TextView tvResultMessage;
    ImageView ivPaymentThumb;
    TextView tvPaymentName;
    ImageView ivCardIssuerThumb;
    TextView tvCardIssuerName;

    public ResultFragment() {
    }

    public static ResultFragment newInstance(ResultModel resultmodel) {

        Bundle args = new Bundle();
        args.putParcelable(ARG_RESULT_MODEL, Parcels.wrap(resultmodel));

        ResultFragment fragment = new ResultFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.Dialog_FullScreen);
        setCancelable(false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_result, container, false);

        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        resultModel = Parcels.unwrap(getArguments().getParcelable(ARG_RESULT_MODEL));
        findAndInitializeViews();
        loadDataResultToViews();
    }

    @Override
    public void findAndInitializeViews() {
        this.btnFinish = view.findViewById(R.id.btn_finish);
        this.tvCardIssuerName = view.findViewById(R.id.tv_card_issuer_name);
        this.tvPaymentName = view.findViewById(R.id.tv_payment_name);
        this.tvResultMessage = view.findViewById(R.id.tv_result_message);
        this.ivCardIssuerThumb = view.findViewById(R.id.iv_card_issuer_thumb);
        this.ivPaymentThumb = view.findViewById(R.id.iv_payment_thumb);

        this.btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    private void loadDataResultToViews(){
        this.tvResultMessage.setText("Estas Pagando\n"+resultModel.getPayerCost().getMessage());
        this.tvPaymentName.setText(resultModel.getPaymentMethodModel().getName());
        this.tvCardIssuerName.setText(resultModel.getCardIssuerModel().getName());
        loadImage(resultModel.getCardIssuerModel().getThumbnail(), ivCardIssuerThumb);
        loadImage(resultModel.getPaymentMethodModel().getThumbnail(), ivPaymentThumb);


    }

    private void loadImage(String imageUrl, ImageView imageView) {
        Glide.with(view.getContext())
                .load(imageUrl)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .placeholder(R.drawable.ic_account_balance)
                .error(R.drawable.ic_account_balance)
                .crossFade(300)
                .into(imageView);
    }

    @Override
    public void onResume() {
        Window window = getDialog().getWindow();
        Point size = new Point();
        // Store dimensions of the screen in `size`
        Display display = window.getWindowManager().getDefaultDisplay();
        display.getSize(size);
        // Set the width of the dialog proportional to 75% of the screen width
        window.setLayout((int) (size.x * 0.90), WindowManager.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        super.onResume();
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
//        getDialog().getWindow()
//                .getAttributes().windowAnimations = R.style.Animation_WindowSlideUpDown;
    }
}

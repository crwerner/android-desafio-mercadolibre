package com.crwerner.desafiomercadolibre.paymentmethods.view.adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.crwerner.desafiomercadolibre.R;
import com.crwerner.desafiomercadolibre.paymentmethods.model.PaymentMethodModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cris on 6/4/18.
 */

public class PaymentMethodAdapter extends RecyclerView.Adapter<PaymentMethodAdapter.ViewHolder>{
    List<PaymentMethodModel> paymentMethodModels;
    Listener listener;
    View view;

    public PaymentMethodAdapter() {
        paymentMethodModels = new ArrayList<>();
    }


    public PaymentMethodAdapter(List<PaymentMethodModel> paymentMethodModels) {
        this.paymentMethodModels = paymentMethodModels;
    }

    public void setPaymentMethodModels(List<PaymentMethodModel> paymentMethodModels) {
        this.paymentMethodModels = paymentMethodModels;
        notifyDataSetChanged();
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view= LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_payment_method, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final PaymentMethodModel model = paymentMethodModels.get(position);

        Glide.with(view.getContext()).load(model.getThumbnail())
                .placeholder(R.drawable.ic_payment)
                .error(R.drawable.ic_payment)
                .crossFade(300)
                .into(holder.ivPaymentThumb);

        holder.tvPaymentName.setText(model.getName());
        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(listener!=null){
                    listener.onItemSelected(model);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return paymentMethodModels.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        CardView container;
        TextView tvPaymentName;
        ImageView ivPaymentThumb;
        public ViewHolder(View itemView) {
            super(itemView);
            container =itemView.findViewById(R.id.container);
            tvPaymentName =itemView.findViewById(R.id.tv_payment_name);
            ivPaymentThumb =itemView.findViewById(R.id.iv_payment_thumb);
        }
    }

    public interface Listener{
        void onItemSelected(PaymentMethodModel paymentMethodModel);
    }


}

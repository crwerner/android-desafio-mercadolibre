package com.crwerner.desafiomercadolibre.paymentmethods.presenter;

import android.support.annotation.NonNull;

import com.crwerner.core.Callback;
import com.crwerner.core.MercadoPagoCore;
import com.crwerner.core.Result;
import com.crwerner.core.exception.MercadoPagoException;
import com.crwerner.core.models.PaymentMethodResponse;
import com.crwerner.desafiomercadolibre.util.MapperUtil;
import com.crwerner.desafiomercadolibre.paymentmethods.model.PaymentMethodModel;
import com.crwerner.desafiomercadolibre.paymentmethods.view.IPaymentMethodsView;

import java.util.List;

/**
 * Created by cris on 6/4/18.
 */

public class PaymentMethodsPresenter extends MapperUtil<PaymentMethodModel, PaymentMethodResponse> implements IPaymentMethodsPresenter {
    private IPaymentMethodsView iPaymentMethodsView;

    public PaymentMethodsPresenter(@NonNull IPaymentMethodsView iPaymentMethodsView) {
        this.iPaymentMethodsView = iPaymentMethodsView;
    }

    @Override
    public void loadPaymentMethods() {
        if(iPaymentMethodsView!=null)
            this.iPaymentMethodsView.showProgress();
        MercadoPagoCore.getInstance().getApiClient().getPaymentMethodCreditCard(new Callback<List<PaymentMethodResponse>>() {
            @Override
            public void success(Result<List<PaymentMethodResponse>> result) {
                if(iPaymentMethodsView!=null) {
                    iPaymentMethodsView.hideProgress();
                    iPaymentMethodsView.onLoadPaymentMethodsSuccess(transform(result.data));
                }

            }

            @Override
            public void failure(MercadoPagoException ex) {
                if(iPaymentMethodsView!=null) {
                    iPaymentMethodsView.onLoadPaymentMethodsFailure(ex.getLocalizedMessage());
                    iPaymentMethodsView.hideProgress();
                }
            }
        });
    }

    @Override
    public PaymentMethodModel transform(PaymentMethodResponse source) {
        PaymentMethodModel ret = new PaymentMethodModel();
        ret.setName(source.name);
        ret.setThumbnail(source.thumbnail);
        ret.setId(source.id);
        ret.setPaymentType(source.paymentTypeId);
        return ret;
    }
}

package com.crwerner.desafiomercadolibre.paymentmethods.model;

import org.parceler.Parcel;

/**
 * Created by cris on 6/4/18.
 */
@Parcel
public class PaymentMethodModel {
    String id;

    String name;

    String paymentType;

    String thumbnail;

    public String getId() {
        return id;
    }

    public String getName() {
        return name!=null?name:"";
    }

    public String getPaymentType() {
        return paymentType;
    }

    public String getThumbnail() {

        return thumbnail!=null?thumbnail:"";
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }


}

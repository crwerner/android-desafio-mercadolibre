package com.crwerner.desafiomercadolibre.paymentmethods.presenter;

/**
 * Created by cris on 6/4/18.
 */

public interface IPaymentMethodsPresenter  {
    void loadPaymentMethods();

}

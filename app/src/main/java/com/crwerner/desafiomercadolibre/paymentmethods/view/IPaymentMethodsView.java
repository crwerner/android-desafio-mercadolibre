package com.crwerner.desafiomercadolibre.paymentmethods.view;

import com.crwerner.core.models.PaymentMethodResponse;
import com.crwerner.desafiomercadolibre.IView;
import com.crwerner.desafiomercadolibre.paymentmethods.model.PaymentMethodModel;

import java.util.List;

/**
 * Created by cris on 6/4/18.
 */

public interface IPaymentMethodsView extends IView{
    void onLoadPaymentMethodsFailure(String message);
    void onLoadPaymentMethodsSuccess(List<PaymentMethodModel> paymentMethodModelList);
    void onEmptyData();
}

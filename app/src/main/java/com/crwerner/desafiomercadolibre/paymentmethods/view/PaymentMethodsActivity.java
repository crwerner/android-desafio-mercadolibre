package com.crwerner.desafiomercadolibre.paymentmethods.view;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.crwerner.desafiomercadolibre.BaseActivity;
import com.crwerner.desafiomercadolibre.R;
import com.crwerner.desafiomercadolibre.cardissuers.view.CardIssuersActivity;
import com.crwerner.desafiomercadolibre.controller.AlertController;
import com.crwerner.desafiomercadolibre.paymentmethods.model.PaymentMethodModel;
import com.crwerner.desafiomercadolibre.paymentmethods.presenter.PaymentMethodsPresenter;
import com.crwerner.desafiomercadolibre.paymentmethods.view.adapter.PaymentMethodAdapter;

import java.util.List;

/**
 * Created by cris on 6/4/18.
 */

public class PaymentMethodsActivity extends BaseActivity implements IPaymentMethodsView {
    private final static String TAG = "PaymentMethodsActivity";
    private static final String ARG_AMOUNT = "amount";
    private static final int REQ_CARD_ISSUERS = 100;
    PaymentMethodsPresenter presenter;
    PaymentMethodAdapter adapter;

    RecyclerView recyclerList;
    ProgressBar progressList;
    Toolbar toolbar;
    AlertController alertController;

    double amount;

    public static Intent getCallingIntent(Context context, double amount){
        Intent intent = new Intent(context, PaymentMethodsActivity.class);
        intent.putExtra(ARG_AMOUNT, amount);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_methods);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setupAnimation();
        }
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        findAndInitializeViews();

        if(savedInstanceState!=null){
            this.amount = savedInstanceState.getDouble(ARG_AMOUNT,0);
        }else{
            this.amount = getIntent().getDoubleExtra(ARG_AMOUNT,0);
        }

        this.presenter = new PaymentMethodsPresenter(this);
        this.presenter.loadPaymentMethods();

    }

    @Override
    public void findAndInitializeViews() {
        this.toolbar = findViewById(R.id.toolbar);
        this.recyclerList = findViewById(R.id.recycler_list);
        this.progressList = findViewById(R.id.progress_list);
        this.alertController = findViewById(R.id.alert_controller);

        progressList.setVisibility(View.GONE);
        setupToolbar();
        setupRecycler();
    }

    @Override
    public void showProgress() {
        progressList.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressList.setVisibility(View.GONE);
    }



    @Override
    public void onLoadPaymentMethodsFailure(String message) {
        recyclerList.setVisibility(View.GONE);
        alertController.setMessage(getString(R.string.alert_payment_method_error))
                .showActionButton(true)
                .configureAction( getString(R.string.retry), new AlertController.Listener() {
                    @Override
                    public void onActionListener() {
                        presenter.loadPaymentMethods();
                        alertController.hide();
                    }
                }).show();
    }

    @Override
    public void onLoadPaymentMethodsSuccess(List<PaymentMethodModel> paymentMethodModelList) {
        recyclerList.setVisibility(View.VISIBLE);
        this.adapter.setPaymentMethodModels(paymentMethodModelList);
        this.alertController.hide();

    }

    @Override
    public void onEmptyData() {
        this.alertController.setMessage(getString(R.string.alert_payment_method_empty)).showActionButton(false).show();
    }

    private void setupRecycler(){
        this.adapter = new PaymentMethodAdapter();
        this.adapter.setListener(new PaymentMethodAdapter.Listener() {
            @Override
            public void onItemSelected(PaymentMethodModel paymentMethodModel) {
                openCardIssuers(paymentMethodModel);

            }
        });
        this.recyclerList.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        this.recyclerList.setAdapter(adapter);
    }

    private void setupToolbar(){

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        toolbar.setTitle(R.string.title_payment_methods);
    }

    private void openCardIssuers(PaymentMethodModel paymentMethod){
        Intent intent = CardIssuersActivity.getCallingIntent(this, paymentMethod, amount);
        transitionForResultTo(intent, REQ_CARD_ISSUERS);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK){
            if(requestCode == REQ_CARD_ISSUERS){
                setResult(RESULT_OK, data);
                finish();
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putDouble (ARG_AMOUNT, amount);

        super.onSaveInstanceState(outState);

    }


}

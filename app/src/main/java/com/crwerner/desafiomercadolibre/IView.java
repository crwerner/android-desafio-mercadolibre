package com.crwerner.desafiomercadolibre;

/**
 * Created by cris on 6/4/18.
 */

public interface IView {
    void findAndInitializeViews();
    void showProgress();
    void hideProgress();
}

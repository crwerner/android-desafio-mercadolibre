package com.crwerner.desafiomercadolibre.cardissuers.view;

import com.crwerner.desafiomercadolibre.IView;
import com.crwerner.desafiomercadolibre.cardissuers.model.CardIssuerModel;

import java.util.List;

/**
 * Created by cris on 7/4/18.
 */

public interface ICardIssuersView extends IView {
    void onLoadCardIssuersSuccess(List<CardIssuerModel> cardIssuerModelList);
    void onLoadCardIssuersFailure(String message);
    void onEmptyData();
}

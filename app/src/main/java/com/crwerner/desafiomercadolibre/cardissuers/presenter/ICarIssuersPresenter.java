package com.crwerner.desafiomercadolibre.cardissuers.presenter;

/**
 * Created by cris on 7/4/18.
 */

public interface ICarIssuersPresenter {
    void loadCardIssuers(String paymentMethodId);
}

package com.crwerner.desafiomercadolibre.cardissuers.view;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.crwerner.desafiomercadolibre.BaseActivity;
import com.crwerner.desafiomercadolibre.R;
import com.crwerner.desafiomercadolibre.cardissuers.model.CardIssuerModel;
import com.crwerner.desafiomercadolibre.cardissuers.presenter.CardIssuersPresenter;
import com.crwerner.desafiomercadolibre.cardissuers.view.adapter.CarIssuersAdapter;
import com.crwerner.desafiomercadolibre.controller.AlertController;
import com.crwerner.desafiomercadolibre.installments.view.InstallmentsActivity;
import com.crwerner.desafiomercadolibre.paymentmethods.model.PaymentMethodModel;
import com.crwerner.desafiomercadolibre.paymentmethods.view.PaymentMethodsActivity;
import com.crwerner.desafiomercadolibre.paymentmethods.view.adapter.PaymentMethodAdapter;

import org.parceler.Parcels;

import java.util.List;

/**
 * Created by cris on 7/4/18.
 */

public class CardIssuersActivity extends BaseActivity implements ICardIssuersView{
    private final static String ARG_PAYMENT_METHOD= "payment_method";
    private final static String ARG_AMOUNT= "amount";
    private static final int REQ_INSTALLMENTS = 101;
    CardIssuersPresenter presenter;

    RecyclerView recyclerList;
    ProgressBar progressList;
    Toolbar toolbar;
    AlertController alertController;
    CarIssuersAdapter adapter;
    PaymentMethodModel paymentMethod;
    double amount;

    public static Intent getCallingIntent(Context context, PaymentMethodModel paymentMethodModel, double amount){
        Intent intent = new Intent(context, CardIssuersActivity.class);

        intent.putExtra(ARG_PAYMENT_METHOD, Parcels.wrap(paymentMethodModel));
        intent.putExtra(ARG_AMOUNT, amount);
        return intent;
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_issuers);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setupAnimation();
        }

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        findAndInitializeViews();

        loadDataFromIntentExtras(savedInstanceState);

        this.presenter = new CardIssuersPresenter(this);
        this.presenter.loadCardIssuers(paymentMethod.getId());

    }

    private void loadDataFromIntentExtras(@Nullable Bundle savedInstanceState) {
        if(savedInstanceState!=null) {
            this.paymentMethod = Parcels.unwrap(savedInstanceState.getParcelable(ARG_PAYMENT_METHOD));
            this.amount = savedInstanceState.getDouble(ARG_AMOUNT, 0);
        }else{
            this.paymentMethod = Parcels.unwrap(getIntent().getParcelableExtra(ARG_PAYMENT_METHOD));
            this.amount = getIntent().getDoubleExtra(ARG_AMOUNT, 0);
        }
    }

    @Override
    public void findAndInitializeViews() {
        this.alertController = findViewById(R.id.alert_controller);
        this.toolbar = findViewById(R.id.toolbar);
        this.recyclerList = findViewById(R.id.recycler_list);
        this.progressList = findViewById(R.id.progress_list);

        progressList.setVisibility(View.GONE);

        setupToolbar();
        setupRecycler();
    }

    @Override
    public void showProgress() {
        this.progressList.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        this.progressList.setVisibility(View.GONE);
    }

    private void setupRecycler(){
        this.adapter = new CarIssuersAdapter();
        this.adapter.setListener(new CarIssuersAdapter.Listener() {
            @Override
            public void onItemSelected(CardIssuerModel cardIssuerModel) {
                openInstallmentActivity(cardIssuerModel);
            }
        });

        this.recyclerList.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        this.recyclerList.setAdapter(adapter);
    }

    private void setupToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        toolbar.setTitle(R.string.title_card_issuers);
    }

    @Override
    public void onLoadCardIssuersSuccess(List<CardIssuerModel> cardIssuerModelList) {
        this.recyclerList.setVisibility(View.VISIBLE);
        this.adapter.setCardIssuerModelList(cardIssuerModelList);
        this.alertController.hide();
    }

    @Override
    public void onLoadCardIssuersFailure(String message) {
        alertController.setMessage(getString(R.string.alert_card_issuers_error))
        .showActionButton(true)
        .configureAction( getString(R.string.retry), new AlertController.Listener() {
            @Override
            public void onActionListener() {
                presenter.loadCardIssuers(paymentMethod.getId());
                alertController.hide();
            }
        }).show();
    }

    @Override
    public void onEmptyData() {
        alertController.setMessage(getString(R.string.alert_card_issuers_empty))
                .showActionButton(false)
                .show();

    }


    private void openInstallmentActivity(CardIssuerModel cardIssuerModel){
        Intent intent = InstallmentsActivity.getCallingInent(this, amount, paymentMethod, cardIssuerModel);
        transitionForResultTo(intent, REQ_INSTALLMENTS);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK){
            if(requestCode == REQ_INSTALLMENTS){
                setResult(RESULT_OK, data);
                finish();
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {

        outState.putParcelable(ARG_PAYMENT_METHOD, Parcels.wrap(paymentMethod));
        outState.putDouble(ARG_AMOUNT, amount);
        super.onSaveInstanceState(outState);
    }
}

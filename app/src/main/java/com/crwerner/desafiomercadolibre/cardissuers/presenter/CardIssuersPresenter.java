package com.crwerner.desafiomercadolibre.cardissuers.presenter;

import android.util.Log;

import com.crwerner.core.Callback;
import com.crwerner.core.MercadoPagoCore;
import com.crwerner.core.Result;
import com.crwerner.core.exception.MercadoPagoException;
import com.crwerner.core.models.CardIssuersResponse;
import com.crwerner.desafiomercadolibre.cardissuers.model.CardIssuerModel;
import com.crwerner.desafiomercadolibre.cardissuers.view.ICardIssuersView;
import com.crwerner.desafiomercadolibre.util.MapperUtil;

import java.util.List;

/**
 * Created by cris on 7/4/18.
 */

public class CardIssuersPresenter  extends MapperUtil<CardIssuerModel, CardIssuersResponse> implements ICarIssuersPresenter {

    private static final String TAG = "CardIssuersPresenter";
    ICardIssuersView issuersView;

    public CardIssuersPresenter(ICardIssuersView issuersView) {
        this.issuersView = issuersView;
    }

    @Override
    public void loadCardIssuers(String paymentMethodId) {
        if(issuersView!=null)issuersView.showProgress();

        MercadoPagoCore.getInstance().getApiClient().getCardIssuers(paymentMethodId, new Callback<List<CardIssuersResponse>>() {
            @Override
            public void success(Result<List<CardIssuersResponse>> result) {
                if(issuersView!=null){
                    if(!result.data.isEmpty()) {
                        issuersView.onLoadCardIssuersSuccess(transform(result.data));
                    }else{
                        issuersView.onEmptyData();
                    }
                    issuersView.hideProgress();
                }
            }

            @Override
            public void failure(MercadoPagoException ex) {
                Log.d(TAG, "error: " + ex.getLocalizedMessage());
                if(issuersView!=null){
                    issuersView.hideProgress();
                    issuersView.onLoadCardIssuersFailure(ex.getLocalizedMessage());
                }
            }
        });

    }

    @Override
    public CardIssuerModel transform(CardIssuersResponse source) {
        CardIssuerModel ret = new CardIssuerModel();
        ret.setName(source.name);
        ret.setThumbnail(source.secureThumbnail);
        ret.setId(source.id);
        return ret;
    }
}

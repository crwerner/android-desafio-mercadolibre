package com.crwerner.desafiomercadolibre.cardissuers.view.adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.crwerner.desafiomercadolibre.R;
import com.crwerner.desafiomercadolibre.cardissuers.model.CardIssuerModel;
import com.crwerner.desafiomercadolibre.paymentmethods.model.PaymentMethodModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cris on 6/4/18.
 */

public class CarIssuersAdapter extends RecyclerView.Adapter<CarIssuersAdapter.ViewHolder>{
    List<CardIssuerModel> cardIssuerModelList;
    Listener listener;
    View view;

    public CarIssuersAdapter() {
        cardIssuerModelList = new ArrayList<>();
    }


    public CarIssuersAdapter(List<CardIssuerModel> cardIssuerModelList) {
        this.cardIssuerModelList = cardIssuerModelList;
    }

    public void setCardIssuerModelList(List<CardIssuerModel> cardIssuerModelList) {
        this.cardIssuerModelList = cardIssuerModelList;
        notifyDataSetChanged();
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view= LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_card_issuer, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final CardIssuerModel model = cardIssuerModelList.get(position);

        Glide.with(view.getContext()).load(model.getThumbnail())
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .placeholder(R.drawable.ic_account_balance)
                .error(R.drawable.ic_account_balance)
                .crossFade(300)
//                .fitCenter()
                .into(holder.ivIssuerThumb);

        holder.tvIssuerName.setText(model.getName());
        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(listener!=null){
                    listener.onItemSelected(model);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return cardIssuerModelList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        CardView container;
        TextView tvIssuerName;
        ImageView ivIssuerThumb;
        public ViewHolder(View itemView) {
            super(itemView);
            container =itemView.findViewById(R.id.container);
            tvIssuerName =itemView.findViewById(R.id.tv_issuer_name);
            ivIssuerThumb =itemView.findViewById(R.id.iv_issuer_thumb);
        }
    }

    public interface Listener{
        void onItemSelected(CardIssuerModel cardIssuerModel);
    }


}

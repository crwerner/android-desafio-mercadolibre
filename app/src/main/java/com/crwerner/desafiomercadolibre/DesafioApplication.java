package com.crwerner.desafiomercadolibre;

import android.app.Application;
import android.content.Context;

import com.crwerner.core.MercadoPago;
import com.crwerner.core.MercadoPagoAuthConfig;
import com.crwerner.core.MercadoPagoConfig;

/**
 * Created by cris on 6/4/18.
 */

public class DesafioApplication extends Application {

    private final static String CLIENT_API_KEY ="444a9ef5-8a6b-429f-abdf-587639155d88";
    Context appContext;


    @Override
    public void onCreate() {
        super.onCreate();
        appContext = this.getApplicationContext();
        MercadoPagoAuthConfig authConfig = new MercadoPagoAuthConfig(CLIENT_API_KEY);
        MercadoPagoConfig config = new MercadoPagoConfig.Builder(appContext)
                .authConfiguration(authConfig).builder();
//
        MercadoPago.initialize(config);
    }
}

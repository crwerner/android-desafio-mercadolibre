package com.crwerner.desafiomercadolibre.controller;

import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.crwerner.desafiomercadolibre.R;

/**
 * Created by cris on 8/4/18.
 */

public class AlertController extends FrameLayout {
    View content;
    ImageView icon;
    TextView message;
    Button btnAction;

    public AlertController(@NonNull Context context) {
        super(context);
        initView();
    }

    public AlertController(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public AlertController(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public AlertController(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initView();
    }

    private void initView(){
        View layout = inflate(getContext(), R.layout.include_alert, null);
        content = layout.findViewById(R.id.content_alert);
        icon = layout.findViewById(R.id.alert_icon);
        message = layout.findViewById(R.id.alert_message);
        btnAction = layout.findViewById(R.id.btn_alert_action);
        this.addView(layout);
    }

    public AlertController setMessage(String message){
        this.message.setText(message);
        return this;
    }

    public void show(){
        content.setVisibility(VISIBLE);
    }

    public void hide(){
        content.setVisibility(GONE);
    }

    public AlertController showActionButton(boolean showAction){
        this.btnAction.setVisibility(showAction?VISIBLE:GONE);
        return this;
    }

    public AlertController configureAction(String actionText, final Listener listener){

        this.btnAction.setText(actionText);
        this.btnAction.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if(listener!=null) {
                    listener.onActionListener();
                }
            }
        });
        return this;
    }

    public interface Listener{
        void onActionListener();
    }

}
